import { LightningElement, wire, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

export default class ArchivedEmailComponent extends LightningElement {

    @api accountId;
    @api contactId;

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('detailArchivedEmailFromAdvanced', this.handleDetailArchivedEmailFromAdvanced, this);
        registerListener('detailArchivedEmail', this.handleDetailArchivedEmail, this);
        registerListener('accountDetails', this.handleContactDetails, this);
        registerListener('showToast', this.handleShowToast, this);
        registerListener('hideToast', this.handleHideToast, this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleShowToast(param) {
        this.showNotification(param);
    }

    handleHideToast() {
        let toast = this.template.querySelector('c-custom-toast');
        toast.closeToast();
    }

    showNotification(param) {
        let toast = this.template.querySelector('c-custom-toast');
        toast.showToast(param);
    }

    handleDetailArchivedEmail(param) {
        const detailArchivedEmailEvt = new CustomEvent('archivedemailclicked', {
            detail: { param }
        });
 
        this.dispatchEvent(detailArchivedEmailEvt); 
    }

    handleDetailArchivedEmailFromAdvanced(param) {
        const detailArchivedEmailEvt = new CustomEvent('archivedemailclickedfromadvancedsearch', {
            detail: { param }
        });
 
        this.dispatchEvent(detailArchivedEmailEvt); 
    }

    handleContactDetails(param) {
        
        const detailArchivedEmailEvt = new CustomEvent('contactdetailclicked', {
            detail: { param }
        });
 
        this.dispatchEvent(detailArchivedEmailEvt); 
    }
}