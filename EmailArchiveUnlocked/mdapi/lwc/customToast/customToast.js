import { LightningElement, api, wire } from 'lwc';
import {fireEvent } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

/** The delay used to autohide toast. */
const DELAY = 4000;

export default class CustomToast extends LightningElement {

    @wire(CurrentPageReference) pageRef;

    @api title;
    @api message;
    @api variant;
    @api autoClose = false;
    @api autoCloseErrorWarning = false;

    toastClosing;

    variantOptions = [
        { label: 'error', value: 'error' },
        { label: 'warning', value: 'warning' },
        { label: 'success', value: 'success' },
        { label: 'info', value: 'info' },
    ];

    @api
    showToast(param) {

        this.title = param.title;
        this.message = param.message;
        this.variant = param.variant;
        this.autoClose = param.autoClose;

        const toastModel = this.template.querySelector('[data-id="toastModel"]');
        toastModel.className = 'toast-show toast-floating';

        if(this.autoClose) {
            if((this.autoCloseErrorWarning && this.variant !== 'success') || this.variant === 'success' || this.variant === 'info') {
                //eslint-disable-next-line
                setTimeout(() => { 
                    this.closeToast();
                }, DELAY);  
            }
        }
        // while(this.toastClosing) {
        //     //eslint-disable-next-line
        //     requestAnimationFrame(this.showToastAsync());
        // }
    }

    showToastAsync() {
        const toastModel = this.template.querySelector('[data-id="toastModel"]');
        toastModel.className = 'toast-show toast-floating';

        if(this.autoClose) {
            if((this.autoCloseErrorWarning && this.variant !== 'success') || this.variant === 'success' || this.variant === 'warning') {

                //eslint-disable-next-line
                setTimeout(() => { 
                    this.closeToast();
                }, DELAY);  
            }
        }
    }

    @api
    closeToast() {
        const toastModel = this.template.querySelector('[data-id="toastModel"]');
        toastModel.className = 'slds-hide';
        // //eslint-disable-next-line
        // requestAnimationFrame(this.closeToastAsync());
    }

    closeToastAsync() {
        this.toastClosing = true;
        const toastModel = this.template.querySelector('[data-id="toastModel"]');
        toastModel.className = 'toast-hide toast-floating';

        toastModel.addEventListener('transitionend', () => {
            toastModel.className = 'slds-hide';
            this.toastClosing = false;
        });
        fireEvent(this.pageRef, 'toastClosed', null);
    }

    get mainDivClass() { 
        return 'slds-notify slds-notify_toast slds-theme_' + this.variant;
    }

    get messageDivClass() { 
        return 'slds-icon_container slds-icon-utility-' + this.variant + ' slds-icon-utility-success slds-m-right_small slds-no-flex slds-align-top';
    }

    get iconName() {
        return 'utility:'+this.variant;
    }
}