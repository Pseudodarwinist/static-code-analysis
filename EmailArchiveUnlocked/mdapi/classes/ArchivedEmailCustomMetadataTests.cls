@IsTest
public with sharing class ArchivedEmailCustomMetadataTests {
    
    @IsTest
    static void validateGetDefaultLinesForSearchResult() {

        Test.startTest();

        Archived_email_search_configuration__mdt configuration = new Archived_email_search_configuration__mdt(
            EmailSearchResultLines__c = 6,
            ArchivedEmailTypeDefaultValue__c = 'lastWeek'
        );

        ArchivedEmailCustomMetadataController.mockConfigs = new List<Archived_email_search_configuration__mdt>();
        ArchivedEmailCustomMetadataController.mockConfigs.add(configuration);

        System.assertEquals(6, ArchivedEmailCustomMetadataController.getDefaultLinesForSearchResult(), 'Default lines for search result is not correct');

        Test.stopTest();
    }

    @IsTest
    static void validateGetDefaultEmailSearchDates() {
        Test.startTest();

        Archived_email_search_configuration__mdt configuration = new Archived_email_search_configuration__mdt(
            EmailSearchResultLines__c = 6,
            ArchivedEmailTypeDefaultValue__c = 'lastWeek'
        );

        ArchivedEmailCustomMetadataController.mockConfigs = new List<Archived_email_search_configuration__mdt>();
        ArchivedEmailCustomMetadataController.mockConfigs.add(configuration);
        
        System.assertEquals('lastWeek', ArchivedEmailCustomMetadataController.getDefaultEmailSearchDates(), 'Default dates picklist is not correct');

        Test.stopTest();
    }
}