@IsTest
private with sharing class ExternalEmailControllerTests {
    

    @IsTest
    static void validateGetExternalEmail() {

        Test.startTest();

        String whereJsonString = '{"name":"testName","surname":"testSurname","email":"","startDate":"2018-08-14","endDate":"2019-08-15","flightDate":"","ibPlus":"","goldenRecord":"","tkt":"","pnrAmadeus":"","pnrResiber":"","sentDate":"","clientType":"","jobId":null,"flightNumber":"","emailType":"","joiner":"OR"}';

        prepareMockData();
        ExternalEmailController.archivedEmailExternalVisible = true;
        ExternalEmailController.archivedEmailTrackingExternalVisible = true;

        List<EmailDatatableWrapper> data =  ExternalEmailController.getExternalEmail(whereJsonString);

        System.assertEquals(data.size(), ExternalEmailController.archivedEmails.size(), 'Data incorrect');

        Test.stopTest();
    }

    @IsTest
    static void validateGetExternalEmailWithNoVisibility() {

        Test.startTest();

        String whereJsonString = '{"name":"testName","surname":"testSurname","email":"","startDate":"2018-08-14","endDate":"2019-08-15","flightDate":"","ibPlus":"","goldenRecord":"","tkt":"","pnrAmadeus":"","pnrResiber":"","sentDate":"","clientType":"","jobId":null,"flightNumber":"","emailType":"","joiner":"OR"}';

        prepareMockData();
        ExternalEmailController.archivedEmailExternalVisible = false;
        ExternalEmailController.archivedEmailTrackingExternalVisible = false;

        try {
            List<EmailDatatableWrapper> data =  ExternalEmailController.getExternalEmail(whereJsonString);
        }
        catch(ExternalEmailController.ExternalEmailException e) {
            System.assertEquals(e.getMessage(), ExternalEmailController.ERROR_NO_ACCESS);
        }

        Test.stopTest();
    }

    @IsTest
    static void validateGetExternalEmailTrackingWrapped() {
        Test.startTest();

        String whereJsonString = '{"filename__c":"7283808_352723_70_59_105475582_FAKE.eml"}';

        archive_email_tracking__x archivedEmailTracking = new archive_email_tracking__x(
            filename__c = '7283808_352723_70_59_105475582_FAKE.eml',
            bouncedate__c = Datetime.newInstance(2019, 1, 1, 0, 0, 0),
            clickdate__c = Datetime.newInstance(2019, 3, 3, 0, 0, 0),
            complaintdate__c = Datetime.newInstance(2019, 4, 4, 0, 0, 0),
            conversiondate__c = Datetime.newInstance(2019, 5, 5, 0, 0, 0),
            notsentdate__c = Datetime.newInstance(2019, 6, 6, 0, 0, 0),
            opendate__c = Datetime.newInstance(2019, 7, 7, 0, 0, 0),
            unsubdate__c = Datetime.newInstance(2019, 8, 8, 0, 0, 0),
            subid__c = 111,
            jobid__c = 123,
            listid__c = 456,
            subjectline__c = 'TEST EMAIL SUBject',
            contactkey__c = 'accountId'
        );

        ExternalEmailController.archivedEmailExternalVisible = true;
        ExternalEmailController.archivedEmailTrackingExternalVisible = true;
        ExternalEmailController.archivedEmailTrackings = new List<EmailTrackingWrapper>();
        ExternalEmailController.archivedEmailTrackings.add(new EmailTrackingWrapper(archivedEmailTracking));

        List<EmailTrackingWrapper> data =  ExternalEmailController.getExternalEmailTrackingWrapped(whereJsonString);

        System.assertEquals(data.size(), ExternalEmailController.archivedEmailTrackings.size(), 'Data incorrect');

        Test.stopTest();
    }

    @IsTest
    static void validateGetExternalEmailTrackingWrappedWithNoVisibility() {
        Test.startTest();

        String whereJsonString = '{"filename__c":"7283808_352723_70_59_105475582_FAKE.eml"}';

        archive_email_tracking__x archivedEmailTracking = new archive_email_tracking__x(
            filename__c = '7283808_352723_70_59_105475582_FAKE.eml',
            bouncedate__c = Datetime.newInstance(2019, 1, 1, 0, 0, 0),
            clickdate__c = Datetime.newInstance(2019, 3, 3, 0, 0, 0),
            complaintdate__c = Datetime.newInstance(2019, 4, 4, 0, 0, 0),
            conversiondate__c = Datetime.newInstance(2019, 5, 5, 0, 0, 0),
            notsentdate__c = Datetime.newInstance(2019, 6, 6, 0, 0, 0),
            opendate__c = Datetime.newInstance(2019, 7, 7, 0, 0, 0),
            unsubdate__c = Datetime.newInstance(2019, 8, 8, 0, 0, 0),
            subid__c = 111,
            jobid__c = 123,
            listid__c = 456,
            subjectline__c = 'TEST EMAIL SUBject',
            contactkey__c = 'accountId'
        );

        ExternalEmailController.archivedEmailExternalVisible = false;
        ExternalEmailController.archivedEmailTrackingExternalVisible = false;
        ExternalEmailController.archivedEmailTrackings = new List<EmailTrackingWrapper>();
        ExternalEmailController.archivedEmailTrackings.add(new EmailTrackingWrapper(archivedEmailTracking));

        try {
            List<EmailTrackingWrapper> data =  ExternalEmailController.getExternalEmailTrackingWrapped(whereJsonString);
        }
        catch(ExternalEmailController.ExternalEmailException e) {
            System.assertEquals(e.getMessage(), ExternalEmailController.ERROR_NO_ACCESS);
        }

        Test.stopTest();
    }

    @IsTest
    static void validateGetExternalEmailWithEmailTracking() {
        Test.startTest();

        String whereJsonString = '{"name":"testName","surname":"testSurname","email":"","startDate":"2018-08-14","endDate":"2019-08-15","flightDate":"","ibPlus":"","goldenRecord":"","tkt":"","pnrAmadeus":"","pnrResiber":"","sentDate":"","clientType":"","jobId":null,"flightNumber":"","emailType":"","joiner":"OR"}';
        String paginationJsonString = '{"pagenumber":1,"pageSize":6}';

        prepareMockData();
        ExternalEmailController.archivedEmailExternalVisible = true;
        ExternalEmailController.archivedEmailTrackingExternalVisible = true;

        List<EmailDatatableWrapper> data =  ExternalEmailController.getExternalEmailWithEmailTracking(paginationJsonString, whereJsonString);

        System.assertEquals(data.size(), ExternalEmailController.archivedEmails.size(), 'Data incorrect');

        Test.stopTest();
    }

    @IsTest
    static void validateGetExternalEmailWithEmailTrackingWithNoVisibility() {
        Test.startTest();

        String whereJsonString = '{"name":"testName","surname":"testSurname","email":"","startDate":"2018-08-14","endDate":"2019-08-15","flightDate":"","ibPlus":"","goldenRecord":"","tkt":"","pnrAmadeus":"","pnrResiber":"","sentDate":"","clientType":"","jobId":null,"flightNumber":"","emailType":"","joiner":"OR"}';
        String paginationJsonString = '{"pagenumber":1,"pageSize":6}';

        prepareMockData();
        ExternalEmailController.archivedEmailExternalVisible = false;
        ExternalEmailController.archivedEmailTrackingExternalVisible = false;
        
        try {
            List<EmailDatatableWrapper> data =  ExternalEmailController.getExternalEmailWithEmailTracking(paginationJsonString, whereJsonString);
        }
        catch(ExternalEmailController.ExternalEmailException e) {
            System.assertEquals(e.getMessage(), ExternalEmailController.ERROR_NO_ACCESS);
        }

        Test.stopTest();
    }

    private static void prepareMockData() {
        archive_v_email__x archivedEmail = new archive_v_email__x(
            filename__c = '7283808_352723_70_59_105475582_FAKE.eml',
            nombre_st__c = 'TEST_NAME',
            primer_apellido_st__c = 'TEST_SURNAME',
            email__c = 'abc@def.com',
            flight_date__c = Datetime.newInstance(2019, 1, 1, 0, 0, 0),
            eventdate__c = Datetime.newInstance(2019, 2, 2, 0, 0, 0),
            id_golden_record__c = 'ASDFJKLM',
            mercado_comunicaciones_st__c = 'ES',
            seg_communication_language__c = 'ES',
            num_tarjeta_ibp_st__c = 'IBP1234',
            pnr_amadeus__c = 'PNR_AMADEUS',
            pnr_resiber__c = 'PNR_RESIBER',
            tipo_cliente_bl__c = 'IBERIA PLUS',
            tkt__c = 'TKT1234',
            processname__c = '352723_70_59_105475582',
            subjectline__c = 'test email subject',
            contactkey__c = 'accountId',
            flight_id__c = 'IB0000'
        );

        EmailDatatableWrapper wrapper = new EmailDatatableWrapper(archivedEmail);
        ExternalEmailController.archivedEmails = new List<EmailDatatableWrapper>();
        ExternalEmailController.archivedEmails.add(wrapper);
    }
}