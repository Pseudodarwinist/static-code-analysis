({
    handleArchivedEmailClicked: function(cmp, evt, hlp) {

        var archivedEmailID = evt.getParam('param');
        
        var lightningAppExternalEvent = $A.get("e.c:lightningAppExternalEvent");
        lightningAppExternalEvent.setParams({'data':archivedEmailID});
        lightningAppExternalEvent.fire();

    },
    handleAdvancedSearchClicked: function(cmp, evt, hlp) {

        var accountData = evt.getParam('param');

        var lightningAppExternalEvent = $A.get("e.c:lightningAppAdvancedSearchEvent");
        lightningAppExternalEvent.setParams({'data':accountData});
        lightningAppExternalEvent.fire();

    }
})