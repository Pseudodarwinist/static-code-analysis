public with sharing class EmailTrackingWrapper {

    @TestVisible private static List<archive_email_tracking__x> mockedEmailTrackings = null;

    @AuraEnabled
    public String filename {get; set;}
    @AuraEnabled
    public String notSentDate {get; set;}
    @AuraEnabled
    public String openDate {get; set;}
    @AuraEnabled
    public String clickDate {get; set;}
    @AuraEnabled
    public String conversionDate {get; set;}
    @AuraEnabled
    public String bounceDate {get; set;}
    @AuraEnabled
    public String unsubDate {get; set;}
    @AuraEnabled
    public String complaintDate {get; set;}

    public EmailTrackingWrapper(archive_email_tracking__x emailTracking) {

        this.filename = emailTracking.filename__c;
        this.notSentDate = Util.convertDateToHumanReadable(emailTracking.notsentdate__c);
        this.openDate = Util.convertDateToHumanReadable(emailTracking.opendate__c);
        this.clickDate = Util.convertDateToHumanReadable(emailTracking.clickdate__c);
        this.conversionDate = Util.convertDateToHumanReadable(emailTracking.conversiondate__c);
        this.bounceDate = Util.convertDateToHumanReadable(emailTracking.bouncedate__c);
        this.unsubDate = Util.convertDateToHumanReadable(emailTracking.unsubdate__c);
        this.complaintDate = Util.convertDateToHumanReadable(emailTracking.complaintdate__c);
    }
}