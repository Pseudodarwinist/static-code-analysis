@isTest
private with sharing class AWSLinkGeneratorTests {
    
    private static final String ACCESS_KEY = 'AKIA37SVVXBHWP3COUU4';
    private static final String SECRET_ACCESS_KEY = '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';
    private static final String BUCKET = 'cloud-cube-eu';
    private static final String AWS_ROOT_FOLDER = 'iberia';

    private static final Integer LINK_LIFESPAN = 15;

    @isTest
    static void vlidateGetSignedURL(){
        Test.startTest();

        String filename = 'testFile.pdf';

        Datetime now = Datetime.now();
        String url = AWSLinkGenerator.getSignedURL(filename, now);

        String encodedString = EncodingUtil.urlEncode(AWS_ROOT_FOLDER + '/' + filename, 'UTF-8');

        Datetime expireson = now.addMinutes(LINK_LIFESPAN);
        Long expires = expireson.getTime()/1000;
  
        String stringtosign = 'GET\n\n\n' + expires + '\n' + '/' + BUCKET + '/' + encodedString;

        Blob mac = Crypto.generateMac('hmacSHA1', blob.valueof(stringtosign), blob.valueof(SECRET_ACCESS_KEY)); 
        String signed= EncodingUtil.urlEncode(EncodingUtil.base64Encode(mac),'UTF-8');  

        System.assert(url.equals('https://' + BUCKET + '.s3.amazonaws.com/' + encodedString + '?AWSAccessKeyId=' + ACCESS_KEY + '&Expires=' + expires + '&Signature=' + signed), 'URLs are not equal');

        Test.stopTest();
    }
}