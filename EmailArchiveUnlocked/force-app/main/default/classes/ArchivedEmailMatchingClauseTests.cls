@isTest
private with sharing class ArchivedEmailMatchingClauseTests {
    
    @isTest
    static void validateGenerateMatchingString(){
        
        Test.startTest();

        ArchivedEmailMatchingClause matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.contactKey = '000000000000000abc';
        String matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(contactkey__c = \'000000000000000abc\')'), 'ContactKey is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.name = 'testName';
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(nombre_st__c = \'TESTNAME\')'), 'Name is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.surname = 'testSurname';
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(primer_apellido_st__c = \'TESTSURNAME\')'), 'Surname is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.clientType = 'IBERIA PLUS';
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(tipo_cliente_bl__c = \'IBERIA PLUS\')'), 'ClientType is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.email = 'Test@tEst.COM';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(email__c = \'test@test.com\')'), 'Email is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.startDate = '2019-01-01';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(eventdate__c > ' + Util.formatStringDateToCST(matchingClause.startDate).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ')'), 'StartDate is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.endDate = '2019-01-02';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(eventdate__c < ' + Util.formatStringDateToCST(matchingClause.endDate).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ')'), 'EndDate is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.flightDate = '2019-01-03';        
        matchingClauseString = matchingClause.generateMatchingString();
        Date d = Date.valueOf(matchingClause.flightDate);
        DateTime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
        System.assert(matchingClauseString.equals('(flight_date__c = ' 
            + dt.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ')'), 'FlightDate is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.sentDate = '2019-01-04';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(eventdate__c > ' 
            + Util.formatStringDateToCST(matchingClause.sentDate).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' AND eventdate__c < ' +
            + Util.formatStringDateToCST(matchingClause.sentDate).addHours(23).addMinutes(59).addSeconds(59).format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ')'), 'SentDate is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.flightNumber = 'IB0000';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(flight_id__c = \'IB0000\')'), 'FlightNumber is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.goldenRecord = 'AbC1234';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(id_golden_record__c = \'ABC1234\')'), 'Golden record is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.ibPlus = 'IBP9999';        
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(num_tarjeta_ibp_st__c = \'IBP9999\')'), 'IBPlus card is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.tkt = 'tkt4321';     
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(tkt__c = \'TKT4321\')'), 'TKT is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.emailType = 'predeparture';     
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(emailtype__c = \'predeparture\')'), 'EmailType is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.jobID = 12345;    
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(jobid__c = 12345.0)'), 'JobID is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.pnrAmadeus = 'pnrAmadeus123';    
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(pnr_amadeus__c = \'PNRAMADEUS123\')'), 'PnrAmadeus is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.pnrResiber = 'pnrResiber456';    
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(pnr_resiber__c = \'PNRRESIBER456\')'), 'PnrResiber is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.subscriberID = 'ab0011223344556677';    
        matchingClauseString = matchingClause.generateMatchingString();
        System.assert(matchingClauseString.equals('(subid__c = \'ab0011223344556677\')'), 'SubscriberID is not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.contactKey = '000000000000000abc';
        matchingClause.name = 'testName';
        matchingClause.surname = 'testSurname';
        matchingClause.clientType = 'IBERIA PLUS';
        matchingClause.email = 'Test@tEst.com';
        matchingClause.flightNumber = 'IB0000';
        matchingClause.goldenRecord = 'AbC1234';
        matchingClause.ibPlus = 'IBP9999';
        matchingClause.jobID = 12345;
        matchingClause.tkt = 'tkt4321';

        matchingClauseString = matchingClause.generateMatchingString();

        System.assert(matchingClauseString.equals('(email__c = \'test@test.com\' AND id_golden_record__c = \'ABC1234\' AND contactkey__c = \'000000000000000abc\' AND ' + 
                'num_tarjeta_ibp_st__c = \'IBP9999\') AND (tkt__c = \'TKT4321\' AND tipo_cliente_bl__c = \'IBERIA PLUS\' AND jobid__c = 12345.0 AND flight_id__c = \'IB0000\' AND ' + 
                'nombre_st__c = \'TESTNAME\' AND primer_apellido_st__c = \'TESTSURNAME\')'), 'Query with ANDs not equal');

        matchingClause = new ArchivedEmailMatchingClause();
        matchingClause.contactKey = '000000000000000abc';
        matchingClause.name = 'testName';
        matchingClause.surname = 'testSurname';
        matchingClause.clientType = 'IBERIA PLUS';
        matchingClause.email = 'Test@tEst.com';
        matchingClause.flightNumber = 'IB0000';
        matchingClause.goldenRecord = 'AbC1234';
        matchingClause.ibPlus = 'IBP9999';
        matchingClause.jobID = 12345;
        matchingClause.tkt = 'tkt4321';
        matchingClause.joiner = 'OR';

        matchingClauseString = matchingClause.generateMatchingString();

        System.assert(matchingClauseString.equals('(email__c = \'test@test.com\' OR id_golden_record__c = \'ABC1234\' OR contactkey__c = \'000000000000000abc\' OR ' + 
                'num_tarjeta_ibp_st__c = \'IBP9999\') AND (tkt__c = \'TKT4321\' AND tipo_cliente_bl__c = \'IBERIA PLUS\' AND jobid__c = 12345.0 AND flight_id__c = \'IB0000\' AND ' + 
                'nombre_st__c = \'TESTNAME\' AND primer_apellido_st__c = \'TESTSURNAME\')'), 'Query with ORs not equal');



        Test.stopTest();    
    }

    @isTest
    static void validateListPagination(){
        Test.startTest();

        String jsonString = '{"pagenumber":1,"pageSize":6,"numberOfRecords":100}';
        ListPagination pagination = (ListPagination)JSON.deserialize(jsonString, ListPagination.class);

        System.assertEquals(pagination.pagenumber, 1, 'Page number incorrect');
        System.assertEquals(pagination.pageSize, 6, 'Page size incorrect');
        System.assertEquals(pagination.numberOfRecords, 100, 'Number of records incorrect');

        Test.stopTest();
    }
    
}