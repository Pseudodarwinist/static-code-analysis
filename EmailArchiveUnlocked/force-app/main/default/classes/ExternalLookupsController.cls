public with sharing class ExternalLookupsController {
    
    @TestVisible private static List<archive_lookups__x> mockedEmailLookups = null;
    @TestVisible private static Boolean lookupsVisible = null;

    public static final String LOOKUP_TYPE_EMAIL_TYPE = 'emailtype';

    @AuraEnabled(cacheable=true)
    public static List<archive_lookups__x> getExternalLookup(String lookupType) {
        if (lookupsVisible != null ? lookupsVisible : Schema.sObjectType.archive_lookups__x.isAccessible()) {
            return mockedEmailLookups != null ? mockedEmailLookups : [SELECT id__c, value__c FROM archive_lookups__x WHERE id_lookup__c =:lookupType];
        }
        else {
            return null;
        }
    }
}