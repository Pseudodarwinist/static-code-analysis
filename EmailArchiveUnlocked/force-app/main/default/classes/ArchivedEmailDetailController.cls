public with sharing class ArchivedEmailDetailController {

    public final String id {get; set;}

    public ArchivedEmailDetailController() {
        this.id = ApexPages.currentPage().getParameters().get('id');
    }
}