public with sharing class ExternalEmailController {

    @TestVisible private static List<EmailDatatableWrapper> archivedEmails = null;
    @TestVisible private static List<EmailTrackingWrapper> archivedEmailTrackings = null;
    @TestVisible private static Boolean archivedEmailTrackingExternalVisible = null;
    @TestVisible private static Boolean archivedEmailExternalVisible = null;

    public static final String ERROR_NO_ACCESS = 'El usuario no tiene permisos para acceder a los datos externos. Contacte con su administrador.';

    @AuraEnabled(cacheable=true)
    public static List<EmailDatatableWrapper> getExternalEmail(String whereClause) {

        Map<String, Object> whereClauseMap = (Map<String, Object>) JSON.deserializeUntyped(whereClause);
        List<EmailDatatableWrapper> dataTable = new List<EmailDatatableWrapper>();

        String query = 'SELECT';
        String archivedEmailExternal = 'archive_v_email__x';  

        if(archivedEmailExternalVisible != null ? !archivedEmailExternalVisible : !Schema.getGlobalDescribe().get(archivedEmailExternal).getDescribe().isAccessible()) {
            throw new ExternalEmailException(ERROR_NO_ACCESS);
        }

        Map<String, Schema.SObjectField> archivedEmailFields = Schema.getGlobalDescribe().get(archivedEmailExternal).getDescribe().fields.getMap();

        // Get the fields from the describe method and append them to the queryString one by one.
        for(String s : archivedEmailFields.keySet()) {
            query += ' ' + s + ',';
        }

        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0, query.Length()-1);
        }

        // Add FROM statement
        query += ' FROM ' + archivedEmailExternal;

        // Add WHERE statement
        query += ' WHERE ';
        for(String key: whereClauseMap.keySet() ) {
            query += key + ' = \'' + whereClauseMap.get(key) + '\'';
        }

        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0, query.Length()-1);
        }

        try {
            for(archive_v_email__x archivedEmail : Database.query(query)) {
                EmailDatatableWrapper dataTableWrapper = new EmailDatatableWrapper(archivedEmail);
                dataTable.add(dataTableWrapper);
            }
        }
        catch(Exception e) {
            System.debug('error -->  ' + e.getMessage());
        }

        return archivedEmails != null ? archivedEmails : dataTable;
    }

    @AuraEnabled(cacheable=true)
    public static List<EmailTrackingWrapper> getExternalEmailTrackingWrapped(String whereClause) {
        
        List<EmailTrackingWrapper> dataTable = new List<EmailTrackingWrapper>();
        Map<String, Object> whereClauseMap = (Map<String, Object>) JSON.deserializeUntyped(whereClause);

        String query = 'SELECT';
        String archivedEmailTrackingExternal = 'archive_email_tracking__x';

        if(archivedEmailTrackingExternalVisible != null ? !archivedEmailTrackingExternalVisible : !Schema.getGlobalDescribe().get(archivedEmailTrackingExternal).getDescribe().isAccessible()) {
            throw new ExternalEmailException(ERROR_NO_ACCESS);
        }

        Map<String, Schema.SObjectField> archivedEmailTrackingFields = Schema.getGlobalDescribe().get(archivedEmailTrackingExternal).getDescribe().fields.getMap();

        // Get the fields from the describe method and append them to the queryString one by one.
        for(String s : archivedEmailTrackingFields.keySet()) {
            query += ' ' + s + ',';
        }

        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0, query.Length()-1);
        }

        // Add FROM statement
        query += ' FROM ' + archivedEmailTrackingExternal;

        // Add WHERE statement
        query += ' WHERE ';
        for(String key: whereClauseMap.keySet() ) {
            query += key + ' = \'' + whereClauseMap.get(key) + '\'';
        }

        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0, query.Length()-1);
        }

        for(archive_email_tracking__x archivedEmailTracking : Database.query(query)) {
            EmailTrackingWrapper dataTableWrapper = new EmailTrackingWrapper(archivedEmailTracking);
            dataTable.add(dataTableWrapper);
        }

        return archivedEmailTrackings != null ? archivedEmailTrackings : dataTable;
    }

    
    @AuraEnabled(cacheable=true)
    public static List<EmailDatatableWrapper> getExternalEmailWithEmailTracking(String jsonListPagination, String jsonParams) {

        ArchivedEmailMatchingClause whereClause;
        ListPagination pagination;
        List<EmailDatatableWrapper> dataTable = new List<EmailDatatableWrapper>();

        try {
            whereClause = (ArchivedEmailMatchingClause)JSON.deserialize(jsonParams, ArchivedEmailMatchingClause.class);
            pagination = (ListPagination)JSON.deserialize(jsonListPagination, ListPagination.class);
        } catch (Exception e) {
            System.debug('Error parsing JSON objects!' + e.getMessage());
            return dataTable;
        }
        
        String query = 'SELECT';
        String archivedEmailExternal = 'archive_v_email__x';  
        
        if(archivedEmailExternalVisible != null ? !archivedEmailExternalVisible : !Schema.getGlobalDescribe().get(archivedEmailExternal).getDescribe().isAccessible()) {
            throw new ExternalEmailException(ERROR_NO_ACCESS);
        }

        Map<String, Schema.SObjectField> archivedEmailFields = Schema.getGlobalDescribe().get(archivedEmailExternal).getDescribe().fields.getMap();

        // Get the fields from the describe method and append them to the queryString one by one.
        for(String s : archivedEmailFields.keySet()) {
            query += ' ' + s + ',';
        }

        // Strip off the last comma if it exists.
        if (query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0, query.Length()-1);
        }

        // Add FROM statement
        query += ' FROM ' + archivedEmailExternal;

        // Add Where 
        query += ' WHERE ' + whereClause.generateMatchingString();

        // Add LIMIT
        query += ' LIMIT ' + pagination.pageSize + ' OFFSET ' + (pagination.pageSize * (pagination.pagenumber - 1));

        try {
            for(archive_v_email__x archivedEmail : Database.query(query)) {
                EmailDatatableWrapper dataTableWrapper = new EmailDatatableWrapper(archivedEmail);
                dataTable.add(dataTableWrapper);
            }
        }
        catch(Exception e) {
            System.debug('error -->  ' + e.getMessage());
        }

        return archivedEmails != null ? archivedEmails : dataTable;
    }  

    /*
    This function is a fix for the select count in an external object that currently is not providing correct results. 
    The only thing that needs to be addressed is the filter parameter to have an accurate count
    */

    // @AuraEnabled(cacheable = true)  
    // public static Integer getExternalEmailCount(String jsonParams) {

    //     ArchivedEmailMatchingClause whereClause;

    //     try {
    //         whereClause = (ArchivedEmailMatchingClause)JSON.deserialize(jsonParams, ArchivedEmailMatchingClause.class);
            
    //     } catch (Exception e) {
    //         System.debug('Error parsing JSON objects!' + e.getMessage());
    //         return 0;
    //     }

    //     Integer count = 0;
 
    //     String endpoint = 'callout:Heroku_Connect/archive$v_email?';
        
    //     String queryString = '$top=20000'; // 20000 is the maximum count we want

    //     queryString += '&$filter=';

    //     String soqlWhere = whereClause.generateMatchingString();
    //     String filter = soqlWhere.replaceAll('=', 'eq').replaceAll('__c', '').replaceAll('>', 'gt').replaceAll('<', 'lt');

    //     queryString += EncodingUtil.urlEncode(filter, 'UTF-8');
    //     queryString += '&$count=true&$select=id';

    //     endpoint += queryString;

    //     Http http = new Http();
    //     HttpRequest request = new HttpRequest();
    //     request.setEndpoint(endpoint);
    //     request.setMethod('GET');

    //     HttpResponse response = http.send(request);
    //     // If the request is successful, parse the JSON response.
        
    //     if (response.getStatusCode() == 200) {
    //         // Deserialize the JSON string into collections of primitive data types.
    //         Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
    //         // Cast the values in the 'animals' key as a list
    //         count = (Integer) results.get('@odata.count');
    //         System.debug('Received the following count: ' + count);
    //     }
    //     else {
    //         Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
    //         // Cast the values in the 'animals' key as a list
    //         String error = (String) results.get('detail');
    //         System.debug('Received the following error: ' + error);
    //     }


    //     return count;  
    // }  

    public class ExternalEmailException extends Exception {}
}