@IsTest
public with sharing class EmailTrackingWrapperTests {
    
    @IsTest
    static void validateWrapper() {

        Test.startTest();

        Account ac = new Account(name='Test Account');
        insert ac;

        archive_email_tracking__x archivedEmailtracking = createTestArchivedEmail(ac.id);

        EmailTrackingWrapper generatedWrapper = new EmailTrackingWrapper(archivedEmailtracking);

        // Asserts
        System.assertEquals(generatedWrapper.filename, archivedEmailtracking.filename__c, 'Id does not match');
        System.assertEquals(generatedWrapper.bounceDate, Util.convertDateToHumanReadable(archivedEmailtracking.bouncedate__c), 'Bounce date does not match');
        System.assertEquals(generatedWrapper.notSentDate, Util.convertDateToHumanReadable(archivedEmailtracking.notsentdate__c), 'Not sent date does not match');
        System.assertEquals(generatedWrapper.openDate, Util.convertDateToHumanReadable(archivedEmailtracking.opendate__c), 'Open date does not match');
        System.assertEquals(generatedWrapper.clickDate, Util.convertDateToHumanReadable(archivedEmailtracking.clickdate__c), 'Click date does not match');
        System.assertEquals(generatedWrapper.conversionDate, Util.convertDateToHumanReadable(archivedEmailtracking.conversiondate__c), 'Conversion date does not match');
        System.assertEquals(generatedWrapper.unsubDate, Util.convertDateToHumanReadable(archivedEmailtracking.unsubdate__c), 'Unsub date does not match');
        System.assertEquals(generatedWrapper.complaintDate, Util.convertDateToHumanReadable(archivedEmailtracking.complaintdate__c), 'Complaint date does not match');

        Test.stopTest();
    }

    private static archive_email_tracking__x createTestArchivedEmail(String accountId) {

        archive_email_tracking__x archivedEmail = new archive_email_tracking__x(
            filename__c = '7283808_352723_70_59_105475582_FAKE.eml',
            bouncedate__c = Datetime.newInstance(2019, 1, 1, 0, 0, 0),
            clickdate__c = Datetime.newInstance(2019, 3, 3, 0, 0, 0),
            complaintdate__c = Datetime.newInstance(2019, 4, 4, 0, 0, 0),
            conversiondate__c = Datetime.newInstance(2019, 5, 5, 0, 0, 0),
            notsentdate__c = Datetime.newInstance(2019, 6, 6, 0, 0, 0),
            opendate__c = Datetime.newInstance(2019, 7, 7, 0, 0, 0),
            unsubdate__c = Datetime.newInstance(2019, 8, 8, 0, 0, 0),
            subid__c = 111,
            jobid__c = 123,
            listid__c = 456,
            subjectline__c = 'TEST EMAIL SUBject',
            contactkey__c = accountId
        );

        return archivedEmail;
    }
}