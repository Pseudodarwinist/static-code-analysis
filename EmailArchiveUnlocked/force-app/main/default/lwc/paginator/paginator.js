import { LightningElement, api, wire} from 'lwc';  
import { CurrentPageReference } from 'lightning/navigation';
import {fireEvent } from 'c/pubsub';

export default class Paginator extends LightningElement {  
    
    @api totalrecords;  
    @api currentpage;  
    @api pagesize;  
    @api paginationReady;
    @api lastPage;

    @wire(CurrentPageReference) pageRef; 

    get showFirstButton() {  
        if (this.currentpage === 1) {  
            return true;  
        }  
        return false;  
    }  

    get nextButtonDisabled() {  
        return this.lastPage;  
    } 
    
    handlePrevious() {  
        fireEvent(this.pageRef, 'previous', null);
    }  

    handleNext() {  
        fireEvent(this.pageRef, 'next', null);
    }  

    handleFirst() {  
        fireEvent(this.pageRef, 'first', null);
    }  
}