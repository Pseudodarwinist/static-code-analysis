import { LightningElement, track, wire, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent, registerListener, unregisterAllListeners } from 'c/pubsub';
import { getRecord } from 'lightning/uiRecordApi';
import getEmailTypes from '@salesforce/apex/ExternalLookupsController.getExternalLookup';
import getDefaultEmailSearchDates from '@salesforce/apex/ArchivedEmailCustomMetadataController.getDefaultEmailSearchDates';

const FIELDS = [
    'Account.Name',
    'Account.R1_ACC_TXT_Id_Golden_record__c',
    'Account.R1_ACC_TXT_Primary_FF_Card_CCPO__c',
    'Account.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c',
    'Account.PersonEmail'
];

const CONTACT_FIELDS = [
    'Contact.FirstName',
    'Contact.LastName',
    'Contact.Email',
];

export default class ArchivedEmailHeader extends LightningElement {

    firstTime = true;
    orAsJoinerForSearch = true;

    // for advanced searches
    @api accountId;
    @api contactId;
    @track account;
    @track contact;

    // Properties
    @track name = '';
    @track surname = '';
    @track email = '';
    @track startDate = '';
    @track endDate = '';
    @track flightDate = '';
    @track ibPlus = '';
    @track goldenRecord = '';
    @track tkt = '';
    @track pnrAmadeus = '';
    @track pnrResiber = '';
    @track sentDate = '';
    @track clientType = '';
    @track jobID = null;
    @track flightNumber = '';
    @track emailType = '';
    @track searchDates = '';
    @track activeSections = ['A', 'B', 'C', 'D'];
    @track emailTypeOptions;
    @track contactKey;
    @track searchDatesOptions = [
        { label: 'Última semana', value: 'lastWeek' },
        { label: 'Último mes', value: 'lastMonth' },
        { label: 'Últimos seis meses', value: 'last6Months' },
        { label: 'Último año', value: 'lastYear' },
        { label: 'Últimos dos años', value: 'lastTwoYears' },
        { label: 'Introducir fecha manualmente', value: 'notKnown' }
    ];

    @wire(getRecord, { recordId: '$accountId', fields: FIELDS }) wiredAccount({ error, data }) {
        if (data) {
            this.account = data;
            this.error = undefined;

            this.contactKey = this.accountId;
            this.goldenRecord = this.account.fields.R1_ACC_TXT_Id_Golden_record__c.value;
            this.ibPlus = this.account.fields.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c.value ? 
                this.account.fields.R1_ACC_TXT_Primary_Frecuent_Flyer_Card__c.value : (this.account.fields.R1_ACC_TXT_Primary_FF_Card_CCPO__c.value ? 
                this.account.fields.R1_ACC_TXT_Primary_FF_Card_CCPO__c.value : null);
            this.email = this.account.fields.PersonEmail.value;

        } else if (error) {
            this.error = error;
            this.account = undefined;

            //eslint-disable-next-line  
            console.log(JSON.stringify(error));
        }
    }

    @wire(getRecord, { recordId: '$contactId', fields: CONTACT_FIELDS }) wiredContact({ error, data }) {
        if (data) {
            this.contact = data;
            this.error = undefined;

            // this.name = this.contact.fields.FirstName.value;
            // this.surname = this.contact.fields.LastName.value;
            // this.email = this.contact.fields.Email.value;
            
        } else if (error) {
            
            this.error = error;
            this.contact = undefined;
        }
    }

    @api
    get searchDatesVisible() {
        return this.searchDates === 'notKnown';
    }

    @track loading = false;

    // Help texts
    @track nameHelp = 'Nombre del cliente a buscar';
    @track surnameHelp = 'Apellidos del cliente a buscar';
    @track emailHelp = 'Email del cliente a buscar. Ha de estar en el formato nombre@host.dominio';
    @track startDateHelp = 'Fecha de inicio para la búsqueda.';
    @track endDateHelp = 'Fecha de fin para la búsqueda';
    @track flightDateHelp = 'Fecha del vuelo para la búsqueda';
    @track ibPlusHelp = 'Número de tarjeta Iberia Plus del cliente a buscar';
    @track goldenRecordHelp = 'Identificador Golden Record del cliente a buscar';
    @track tktHelp = 'Identificador de ticket';
    @track pnrAmadeusHelp = 'Localizador PNR Amadeus';
    @track pnrResiberHelp = 'Localizador PNR Resiber';
    @track sentDateHelp = 'Fecha de envío del correo al cliente';
    @track jobIdHelp = 'Número de job del archivado de emails';
    @track flightNumberHelp = 'Número de vuelo en formato IB1234';
    @track emailTypeHelp = 'Tipo de email enviado al cliente';
    @track searchDatesHelp = 'Intente acotar la búsqueda lo máximo posible para reducir tiempos de espera';

    @wire(CurrentPageReference) pageRef;
    @wire(getDefaultEmailSearchDates) defaultEmailSearchDate;

    // Email types loaded dynamically
    @wire(getEmailTypes, { lookupType: 'emailtype' }) externalEmailTypes(result) {
        if (result.data) {
            this.emailTypeOptions = [];

            for (let i = 0; i < result.data.length; i++) {
                this.emailTypeOptions[i] = { label: result.data[i].value__c, value: result.data[i].id__c };
            }

            this.emailTypeOptions[result.data.length] = { label: 'Todos', value: '' };
        }
    }

    connectedCallback() {
        registerListener('loadingStart', this.handleLoadingStart, this);
        registerListener('loadingEnd', this.handleLoadingEnd, this);
        registerListener('toastClosed', this.handleToastClosed, this);
    }

    renderedCallback() {
        if (this.firstTime) {
            this.loading = false;
            this.firstTime = false;

            getDefaultEmailSearchDates().then(emailSearchDate => {
                if (emailSearchDate !== undefined) {
                    this.searchDates = emailSearchDate; // Default option

                    // In advanced search, force the user to enter start and end dates
                    if(this.accountId !== undefined && this.accountId !== "") {
                        this.searchDates = 'notKnown';
                    }
                    this.generateDatesFromCombobox();
                }
            })
            .catch(emailSearchDateError => {

                this.sendToast('Error', 'No hay configuración por defecto en la org. Utilizando configuración local', 'warning', false);

                this.searchDates = 'lastWeek'; // Default option
                this.generateDatesFromCombobox();
                //eslint-disable-next-line
                console.error(JSON.stringify(emailSearchDateError));
            });
        }
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleLoadingStart() {
        this.loading = true;
    }

    handleLoadingEnd() {
        this.loading = false;
    }

    handleToastClosed() {
        this.resetValidations();
    }

    // Event handlers
    handlePnrAmadeusChanged(evt) {
        this.pnrAmadeus = evt.target.value === null ? '' : evt.target.value;
    }

    handlePnrResiberChanged(evt) {
        this.pnrResiber = evt.target.value === null ? '' : evt.target.value;
    }

    handleEmailChanged(evt) {
        this.email = evt.target.value === null ? '' : evt.target.value;
    }

    handleSentDateChanged(evt) {
        this.sentDate = evt.target.value === null ? '' : evt.target.value;
    }

    handleJobIdChanged(evt) {
        this.jobID = evt.target.value === null ? '' : evt.target.value;
    }

    handleFlightNumberChanged(evt) {
        this.flightNumber = evt.target.value === null ? '' : evt.target.value;
    }

    handleStartDateChanged(evt) {
        this.startDate = evt.target.value === null ? '' : evt.target.value;
    }

    handleEndDateChanged(evt) {
        this.endDate = evt.target.value === null ? '' : evt.target.value;
    }

    handleFlightDateChanged(evt) {
        this.flightDate = evt.target.value === null ? '' : evt.target.value;
    }

    handleNameChanged(evt) {
        this.name = evt.target.value === null ? '' : evt.target.value;
    }

    handleSurnameChanged(evt) {
        this.surname = evt.target.value === null ? '' : evt.target.value;
    }

    handleIbPlusChanged(evt) {
        this.ibPlus = evt.target.value === null ? '' : evt.target.value;
    }

    handleGoldenRecordChanged(evt) {
        this.goldenRecord = evt.target.value === null ? '' : evt.target.value;
    }

    handleTktChanged(evt) {
        this.tkt = evt.target.value === null ? '' : evt.target.value;
    }

    handleEmailTypeChanged(evt) {
        this.emailType = evt.target.value === null ? '' : evt.target.value;
    }

    handleSearchDatesChanged(evt) {

        this.searchDates = evt.target.value === null ? '' : evt.target.value;
        this.generateDatesFromCombobox(this.searchDates);
    }

    handleContactKeyChanged(evt) {
        this.contactKey = evt.target.value === null ? '' : evt.target.value;
    }

    generateDatesFromCombobox() {

        var tempEndDate = new Date(); // today
        var tempStartDate = new Date(tempEndDate);
        var offsetDays;
        tempEndDate.setDate(tempEndDate.getDate() + 1);
        this.endDate = tempEndDate.toISOString().split("T")[0];

        switch (this.searchDates) {
            case 'lastWeek':
                offsetDays = 7;
                break;
            case 'lastMonth':
                offsetDays = 30;
                break;
            case 'last6Months':
                offsetDays = 180;
                break;
            case 'lastYear':
                offsetDays = 365;
                break;
            case 'lastTwoYears':
                offsetDays = 365 * 2;
                break;
            case 'notKnown':
            default:
                offsetDays = undefined;
                this.startDate = '';
                this.endDate = ''
                break;
        }

        if (offsetDays !== undefined) {
            tempStartDate.setDate(tempStartDate.getDate() - offsetDays);
            this.startDate = tempStartDate.toISOString().split("T")[0];
        }
    }

    /// Button events
    handleExpandAllClick() {
        this.activeSections = ['A', 'B', 'C', 'D'];
    }

    handleCollapseAllClick() {
        this.activeSections = [];
    }

    handleSearchClick() {

        const param = {
            "name": this.name,
            "surname": this.surname,
            "email": this.email,
            "startDate": this.startDate,
            "endDate": this.endDate,
            "flightDate": this.flightDate,
            "ibPlus": this.ibPlus,
            "goldenRecord": this.goldenRecord,
            "tkt": this.tkt,
            "pnrAmadeus": this.pnrAmadeus,
            "pnrResiber": this.pnrResiber,
            "sentDate": this.sentDate,
            "clientType": this.clientType,
            "jobId": this.jobID,
            "flightNumber": this.flightNumber,
            "emailType": this.emailType,
            "contactKey": this.contactKey,
            "joiner": (this.orAsJoinerForSearch === true || (this.accountId !== undefined && this.accountId !== "")) ? "OR":"AND"
        };

        if (this.searchParamsValid()) {
            //eslint-disable-next-line
            console.log("params valid --> " + JSON.stringify(param));
            fireEvent(this.pageRef, 'searchArchivedEmail', param);
        }
    }

    onKeyUp(event) {
        // Handle Enter key
        if (event.keyCode === 13) {
            this.handleSearchClick();
        }
    }

    handleCancelClick() {
        this.email = '';
        this.name = '';
        this.surname = '';
        this.ibPlus = '';
        this.goldenRecord = '';
        this.startDate = '';
        this.endDate = '';
        this.flightDate = '';
        this.tkt = '';
        this.pnrAmadeus = '';
        this.pnrResiber = '';
        this.sentDate = '';
        this.clientType = '';
        this.jobID = null;
        this.subscriberID = '';
        this.flightNumber = '';
        this.emailType = '';
        this.searchDates = 'lastWeek';

        // Close toast if it was opened
        this.closeToast();
        this.resetValidations();

        fireEvent(this.pageRef, 'clearTableData', null);
    }

    // General functions
    findComponentByName(component, name) {
        return [...this.template.querySelectorAll(component)].filter(function (lightningInput) {
            return lightningInput.name === name;
        })[0];
    }

    findInputByName(name) {
        return this.findComponentByName("lightning-input", name);
    }

    findComboboxByName(name) {
        return this.findComponentByName("lightning-combobox", name);
    }


    resetValidations() {

        [...this.template.querySelectorAll("lightning-input")].forEach(cmp => {
            cmp.setCustomValidity('');
            cmp.reportValidity();
        });

        [...this.template.querySelectorAll("lightning-combobox")].forEach(cmp => {
            cmp.setCustomValidity('');
            cmp.reportValidity();
        });
    }

    searchParamsValid() {

        // Reset previous validations
        this.resetValidations();

        // Close toast if it was opened
        this.closeToast();

        // if everything is empty --> invalid
        if (this.startDate === '' && this.endDate === '' && this.name === '' && this.surname === '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate === '' && this.flightNumber === '' 
            && this.jobID === null && this.tkt === '' && this.emailType === '') {
            this.sendToast('Error', 'Introduzca valores para buscar', 'error', false);

            return false
        }

        //if we only have event dates --> invalid
        if ((this.startDate !== '' || this.endDate !== '') && this.name === '' && this.surname === '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate === '' && this.flightNumber === '' && this.jobID === null && this.tkt === ''
            && this.emailType === '') {

            if (this.startDate !== '' || this.endDate !== '') {
                this.findComboboxByName('searchDates').setCustomValidity('Hacen falta más parámetros de búsqueda. Pruebe añadiendo apellido, email, o número de vuelo.');
                this.findComboboxByName('searchDates').reportValidity();
            }

            this.sendToast('Error', 'Hacen falta más parámetros para poder buscar', 'error', false);
            return false;
        }

        // if we only have email type --> invalid
        if (this.name === '' && this.surname === '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate === '' && this.flightNumber === ''  && this.jobID === null
            && this.tkt === '' && this.emailType !== '') {

            this.sendToast('Error', 'Hacen falta más parámetros para poder buscar', 'error', false);

            this.findComboboxByName('emailType').setCustomValidity('Hacen falta más parámetros de búsqueda. Pruebe añadiendo apellido, email, o número de vuelo.');
            this.findComboboxByName('emailType').reportValidity();

            return false
        }

        // if we only have name --> invalid
        if (this.name !== '' && this.surname === '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate === '' && this.flightNumber === ''  && this.jobID === null
            && this.tkt === '' && this.emailType === '') {
            this.sendToast('Error', 'Hacen falta más parámetros para poder buscar', 'error', false);

            this.findInputByName('name').setCustomValidity('Hacen falta más parámetros de búsqueda. Pruebe añadiendo apellido, email, o número de vuelo.');
            this.findInputByName('name').reportValidity();

            return false
        }

        // if we only have surname --> invalid
        if (this.name === '' && this.surname !== '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate === '' && this.flightNumber === ''  && this.jobID === null
            && this.tkt === '' && this.emailType === '') {
            this.sendToast('Error', 'Hacen falta más parámetros para poder buscar', 'error', false);

            this.findInputByName('surname').setCustomValidity('Hacen falta más parámetros de búsqueda. Pruebe añadiendo nombre, email, o número de vuelo.');
            this.findInputByName('surname').reportValidity();

            return false
        }

        // if we only have flight number --> invalid
        if (this.name === '' && this.surname === '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate === '' && this.flightNumber !== '' 
            && this.jobID === null && this.tkt === '' && this.emailType === '') {
            this.sendToast('Error', 'Hacen falta más parámetros para poder buscar', 'error', false);

            this.findInputByName('flightNumber').setCustomValidity('Para buscar por número de vuelo, introduzca también la fecha de vuelo.');
            this.findInputByName('flightNumber').reportValidity();

            return false
        }

        // if we only have flight date --> invalid
        if (this.name === '' && this.surname === '' && this.email === '' && this.ibPlus === '' && this.goldenRecord === ''
            && this.goldenRecord === '' && this.pnrAmadeus === '' && this.pnrResiber === '' && this.flightDate !== '' && this.flightNumber === '' 
            && this.jobID === null && this.tkt === '' && this.emailType === '') {
            this.sendToast('Error', 'Hacen falta más parámetros para poder buscar', 'error', false);

            this.findInputByName('flightDate').setCustomValidity('Para buscar por fecha de vuelo, introduzca algún dato más.');
            this.findInputByName('flightDate').reportValidity();

            return false
        }

        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                inputCmp.reportValidity();
                return validSoFar && inputCmp.checkValidity();
            }, true);

        if (!allValid) {
            this.sendToast('Error', 'Corrija los errores en los campos', 'error', false);
        }

        return allValid;
    }

    sendToast(title, message, variant, autoClose) {
        fireEvent(this.pageRef, 'showToast', { title: title, message: message, variant: variant, autoClose: autoClose });
    }

    closeToast() {
        fireEvent(this.pageRef, 'hideToast', null);
    }
}