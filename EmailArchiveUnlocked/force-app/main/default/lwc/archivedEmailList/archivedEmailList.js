import { LightningElement, track, wire, api } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent, registerListener, unregisterAllListeners } from 'c/pubsub';

// Apex access
import getExternalEmail from '@salesforce/apex/ExternalEmailController.getExternalEmailWithEmailTracking';
import getDefaultPageSize from '@salesforce/apex/ArchivedEmailCustomMetadataController.getDefaultLinesForSearchResult';

// Columns for datatable
const columnsForAccountLayout = [
    {
        type: "button-icon",
        fixedWidth: 40, 
        typeAttributes: {
            iconName: 'utility:page',
            name: 'emailDetails',
            alternativeText: 'Información del email',
            disabled: {fieldName: 'trackingDataNotPresent'}
        }
    },
    { label: 'Fecha Envío', fieldName: 'eventTime', initialWidth: 180},
    { label: 'Email', fieldName: 'email', initialWidth: 250 },
    { label: 'Nombre', fieldName: 'name', initialWidth: 120 }, 
    { label: 'Apellidos', fieldName: 'surname', initialWidth: 160 },
    { label: 'Asunto', fieldName: 'subject', initialWidth: 320},
    { label: 'Fecha de vuelo', fieldName: 'flightDate', initialWidth: 180},
    { label: 'Nº de vuelo', fieldName: 'flightId', initialWidth: 130},
    { label: 'Tipo cliente', fieldName: 'clientType', initialWidth: 140  },
    { label: 'Iberia plus', fieldName: 'iberiaPlus', initialWidth: 140  },
    { label: 'TKT', fieldName: 'tkt', initialWidth: 140  },
    { label: 'PNR Resiber', fieldName: 'pnrResiber', initialWidth: 140  },
    { label: 'PNR Amadeus', fieldName: 'pnrAmadeus', initialWidth: 140  },
    { label: 'ID Golden record', fieldName: 'goldenRecord', initialWidth: 180 }      
];

const columns = [
    {
        type: "button-icon",
        fixedWidth: 40, 
        typeAttributes: {
            iconName: 'utility:user',
            name: 'clientDetails',
            alternativeText: 'Información del cliente',
            disabled: {fieldName: 'contactDataNotPresent'}
        }
    },
    {
        type: "button-icon",
        fixedWidth: 40, 
        typeAttributes: {
            iconName: 'utility:page',
            name: 'emailDetails',
            alternativeText: 'Información del email',
            disabled: {fieldName: 'trackingDataNotPresent'}
        }
    },
    { label: 'Fecha Envío', fieldName: 'eventTime', initialWidth: 180},
    { label: 'Email', fieldName: 'email', initialWidth: 250 },
    { label: 'Nombre', fieldName: 'name', initialWidth: 120 }, 
    { label: 'Apellidos', fieldName: 'surname', initialWidth: 160 },
    { label: 'Asunto', fieldName: 'subject', initialWidth: 320},
    { label: 'Fecha de vuelo', fieldName: 'flightDate', initialWidth: 180},
    { label: 'Nº de vuelo', fieldName: 'flightId', initialWidth: 130},
    { label: 'Tipo cliente', fieldName: 'clientType', initialWidth: 140  },
    { label: 'Iberia plus', fieldName: 'iberiaPlus', initialWidth: 140  },
    { label: 'TKT', fieldName: 'tkt', initialWidth: 140  },
    { label: 'PNR Resiber', fieldName: 'pnrResiber', initialWidth: 140  },
    { label: 'PNR Amadeus', fieldName: 'pnrAmadeus', initialWidth: 140  },
    { label: 'ID Golden record', fieldName: 'goldenRecord', initialWidth: 180 }       
];

export default class ArchivedEmailList extends LightningElement {
    @api
    get hideCheckBoxColumn () {
        return true;
    }

    get columns () {
        if(this.accountData === undefined && this.accountId === "") {
            return columns;
        }
        return columnsForAccountLayout;
    }

    get dataReady() {
        if(this.data === undefined || this.data.length === 0) {
            return false;
        }
        return true;
    }

    firstTime = true;
    firstTimeSearch = false;

    @api contactId;
    @api accountId;

    @track jsonParams = undefined;
    @track data = undefined;
    @track sortedBy;
    @track sortedDirection = 'asc';
    @track loading = true;
    @track relatedLoading = false;

    @api totalpages;
    @api currentpage;
    @api pagesize;
    @api lastPage;
    @api accountData;
    @api paginationReady = false;

    searching = false;

    @wire(CurrentPageReference) pageRef;

    handleRowActions(event) {

        let actionName = event.detail.action.name;

        let row = event.detail.row;

        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'emailDetails':
                this.viewEmailDetails(row);
                break;
            case 'clientDetails':
                this.viewClienDetails(row);
                break;
        }
    }

    handlePrevious() {  
        if (this.currentpage > 1) {  
            this.startLoading();
            fireEvent(this.pageRef, 'loadingStart', null);
            this.currentpage = this.currentpage - 1;
            this.startSearching();
        }  
    }  

    handleNext() {
        if (this.currentpage) {
            this.startLoading();
            fireEvent(this.pageRef, 'loadingStart', null);
            this.currentpage = this.currentpage + 1;  
            this.startSearching();
        }
    } 

    handleFirst() {  
        this.startLoading();
        fireEvent(this.pageRef, 'loadingStart', null);
        this.currentpage = 1;  
        this.startSearching();
    }  

    startSearching() {
        this.searching = true;  
        this.searchForExternalEmail();
    }

    viewEmailDetails(currentRow) {
   
        if(typeof currentRow !== "undefined" ) {

            // The list is used as a related list only, or we are in the advanced search feature
            if(this.accountData !== undefined || this.accountId !== "") {
                this.handleElementTableClicked(JSON.stringify(currentRow))
            }
            else {
                fireEvent(this.pageRef, 'detailArchivedEmail', JSON.stringify(currentRow));
            }
        } 
    }

    viewClienDetails(currentRow) {
        if(currentRow.contactKey !== null) {
            fireEvent(this.pageRef, "accountDetails", '/' + currentRow.contactKey);
        }
    }

    handleElementTableClicked(param) {

        if(this.accountId !== undefined && this.accountId !== "") {
            fireEvent(this.pageRef, 'detailArchivedEmailFromAdvanced', param);
        }
        else {
            const detailArchivedEmailEvt = new CustomEvent('archivedemailclicked', {
                detail: { param }
            });
    
            this.dispatchEvent(detailArchivedEmailEvt); 
        }
    }

    connectedCallback() {
        registerListener('searchArchivedEmail', this.handleSearchArchivedEmail, this);
        registerListener('clearTableData', this.handleClearTableData, this);
        registerListener('previous', this.handlePrevious, this);
        registerListener('next', this.handleNext, this);
        registerListener('first', this.handleFirst, this);

        getDefaultPageSize().then(defaultPageSize => {  
            if(defaultPageSize !== undefined) {
                this.pagesize = defaultPageSize; // Default option
            }

            // Related list mode
            if (this.accountData !== undefined) {

                this.relatedLoading = true;

                let iberiaPlus = this.accountData.iberiaPlus !== "" ? this.accountData.iberiaPlus : (this.accountData.iberiaPlus2 !== "" ? this.accountData.iberiaPlus2 : null);

                let params = {
                    joiner: 'OR',
                    contactKey: this.accountData.accountId,
                    goldenRecord: this.accountData.goldenRecord,
                    email: this.accountData.email,
                    ibPlus: iberiaPlus,
                    startDate: this.accountData.startDate,
                    endDate: this.accountData.endDate
                };
    
                this.handleSearchArchivedEmail(params);
            }
        })
        .catch(emailSearchDateError => {
            
            this.sendToast('Error', 'No hay configuración por defecto en la org. Utilizando configuración local', 'warning', false);
            
            this.pageSize = 6; // Fallback default option
            //eslint-disable-next-line
            console.error(JSON.stringify(emailSearchDateError));
        });
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
    }

    handleSearchArchivedEmail (param) {

        if (this.searching) {
            return;
        }

        this.firstTimeSearch = true;

        this.data = undefined;
        this.searching = true;
        this.jsonParams = JSON.stringify(param);

        this.startLoading();
        fireEvent(this.pageRef, 'loadingStart', null);

        this.currentpage = 1;
        this.searchForExternalEmail();
    }

    handleAdvancedSearchClick() {

        const param = JSON.stringify(this.accountData);
        const advancedSearchEvent = new CustomEvent('advancedsearchclicked', {
            detail: { param } 
        });
 
        this.dispatchEvent(advancedSearchEvent); 
    }
    
    startLoading () {
        this.loading = true;
    }

    endLoading () {
        this.loading = false;
    }

    sendToast(title, message, variant, autoClose) {
        fireEvent(this.pageRef, 'showToast', {title : title, message : message, variant: variant, autoClose: autoClose}); 
    }

    searchForExternalEmail() {
        
        var jsonListPagination = JSON.stringify({pagenumber: this.currentpage, pageSize: this.pagesize});
        
        //eslint-disable-next-line
        console.log('pagination --> ' + jsonListPagination);
        
        getExternalEmail({jsonListPagination : jsonListPagination, jsonParams : this.jsonParams }).then(externalEmailList => {  
                        
            this.relatedLoading = false;
            this.searching = false;
            fireEvent(this.pageRef, 'loadingEnd', null);
            this.endLoading();

            if(externalEmailList !== undefined && this.currentpage > 1 && (externalEmailList.length === 0 || externalEmailList.length % this.pagesize !== 0)) {
                // last page or no results, disable paginator last button
                this.lastPage = true;

                // Go back if we have a number of results that is divisible by pageSize
                if(externalEmailList.length === 0) { 
                    this.currentpage = this.currentpage - 1;
                }
                
                // Prevent to loose the last page data
                if(externalEmailList.length % this.pagesize !== 0) {
                    this.data = externalEmailList;
                }

                this.sendToast('', 'No hay más resultados', 'info', true)
            }
            else {
                this.data = externalEmailList;
                this.lastPage = false;
                // Show the toast only the first time, not in different pages
                if(this.firstTimeSearch) {
                    this.sendToast('', 'La búsqueda ha sido satisfactoria', 'success', true)
                    this.firstTimeSearch = false;
                }
            }

            if (this.data === undefined || (this.currentpage === 1 && this.data.length === 0)) {
                this.sendToast('', 'No se han encontrado resultados para estos parámetros de búsqueda', 'info', true)
                this.paginationReady = false;
            }
            else {
                this.paginationReady = true;
            }
        })
        .catch(externalEmailError => {
            
            this.relatedLoading = false;
            this.searching = false;
            fireEvent(this.pageRef, 'loadingEnd', null);
            this.endLoading();
            this.paginationReady = false; 
            this.data = undefined;  

            //eslint-disable-next-line
            console.error(JSON.stringify(externalEmailError));

            if(externalEmailError.status === 400) {
                if(externalEmailError.body.exceptionType === 'ExternalEmailController.ExternalEmailException') {
                    this.sendToast('', externalEmailError.body.message, 'error', false);
                }
                else {
                    this.sendToast('', 'La búsqueda no ha funcionado correctamente, probablemente debido a un exceso de tiempo. Pruebe a afinar la búsqueda un poco más', 'warning', true)
                }
            }    
        });
    }


    sortData(fieldName, sortDirection){
        
        var tempData = JSON.parse(JSON.stringify(this.data));
        //function to return the value stored in the field
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        tempData.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });

        //set sorted data to opportunities attribute
        this.data = tempData;
    }

    updateColumnSorting(event){
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        this.sortData(this.sortedBy,this.sortedDirection);       
    }


    handleClearTableData () {
        this.currentpage = 0;
        this.totalpages = 1;  
        this.totalrecords = 0; 
        this.jsonParams = undefined;
        this.data = undefined;
        this.paginationReady = false;
    }
}