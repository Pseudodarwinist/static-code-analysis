import { LightningElement, api, track} from 'lwc';
import getExternalEmailTracking from '@salesforce/apex/ExternalEmailController.getExternalEmailTrackingWrapped';
import getExternalEmail from '@salesforce/apex/ExternalEmailController.getExternalEmail';
import isSuperUser from '@salesforce/apex/ArchivedEmailCustomMetadataController.isSuperUser';


const urlToPdfService = "https://url-to-pdf-api.herokuapp.com/api/render?url=";

export default class ArchivedEmailFullScreenDetail extends LightningElement {

    @api
    set id(value) {
        this.privateId = value;
        this.whereClause = value != null ? JSON.stringify({ 'filename__c' : value }) : null;
    }

    get id() {
        return this.privateId;
    }

    get pdfDocumentURL() {
        return urlToPdfService + encodeURIComponent(this.emailData.externalURLhtml) + '&attachmentName=' + this.emailData.filename + '.pdf';
    }
    
    @track privateId = null
    @track whereClause = null;
    @track emailTrackingData = null;
    @track emailData = null;

    @track loadingEmail = false;
    @track loadingEmailData = false;

    alreadySearched = false;

    connectedCallback() {
        
        isSuperUser().then(superUserData => {
            this.superUser = superUserData;
        })
        .catch(error => {
            //eslint-disable-next-line
            console.error(JSON.stringify(error));
        });

        if (this.whereClause !== null && this.alreadySearched === false) {
            this.alreadySearched = true;

            this.loadingEmail = true;
            this.loadingEmailData = true;

            getExternalEmail({whereClause: this.whereClause }).then(externalEmailList => { 
                if (externalEmailList && externalEmailList.length > 0) { 
                    this.emailData = externalEmailList[0];
                }
                this.loadingEmail = false;
            })
            .catch(externalEmailListError => {
                //eslint-disable-next-line
                console.error(JSON.stringify(externalEmailListError));
            });

            getExternalEmailTracking({whereClause: this.whereClause }).then (externalEmailTrackingList => {
                if(externalEmailTrackingList && externalEmailTrackingList.length > 0) {
                    this.emailTrackingData = externalEmailTrackingList[0];
                }

                this.loadingEmailData = false;
            })
            .catch(externalEmailTrackingListError => {
                //eslint-disable-next-line
                console.error(JSON.stringify(externalEmailTrackingListError));
            });
        }
    }

    handleDownloadPdfFileClick() {
        window.open(this.pdfDocumentURL);  
    }

    handleDownloadOriginalFileClick () {
        window.open(this.emailData.externalURLeml);
    }
}