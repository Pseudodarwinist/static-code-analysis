({
    handleArchivedEmailClicked: function(cmp, evt, hlp) {

        var archivedEmailID = evt.getParam('param');

        var lightningAppExternalEvent = $A.get("e.c:lightningAppExternalEvent");
        lightningAppExternalEvent.setParams({'data':archivedEmailID});
        lightningAppExternalEvent.fire();

    },

    handleOnContactDetailClicked: function(cmp, evt, hlp) {

        var accountId = evt.getParam('param');

        var contactDetailsLightningEvent = $A.get("e.c:contactDetailsLightningEvent");
        contactDetailsLightningEvent.setParams({'data':accountId});
        contactDetailsLightningEvent.fire();

    },

    handleArchivedEmailClickedFromAdvancedSearch: function(cmp, evt, hlp) {

        var archivedEmailID = evt.getParam('param');
        
        var lightningAppExternalEvent = $A.get("e.c:emailDetailFromAdvSearchEvent");
        lightningAppExternalEvent.setParams({'data':archivedEmailID});
        lightningAppExternalEvent.fire();

    }
})